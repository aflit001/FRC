import os,sys

import atexit
from glob   import glob
from pprint import pprint
import shutil

ONLY_PROCESS_IF_DONT_EXISTS = False

##########################
## CHECKING
##########################
env = Environment(ENV = os.environ) # Add os.environ, otherwise PATH doesn't get picked up
env.SourceCode(".", None)
#env.Decider('timestamp-newer')

env.Decider('timestamp-match')
#
Decider('timestamp-match')

def my_decider(dependency, target, prev_ni):
    return not os.path.exists(str(target))
env.Decider(my_decider)

# How many CPU's can we use ?
import multiprocessing
num_cpu = multiprocessing.cpu_count()
if env.GetOption('num_jobs') <= 1:
	env.SetOption('num_jobs', num_cpu)
	num_threads = num_cpu/4
else:
	num_threads = num_cpu/env.GetOption('num_jobs')
print "running with -j", GetOption('num_jobs')
# Set number of simultaneous threads to something less than the number of CPUs
# /CPUs

#############
## Scanner ##
#############
#def bamfile_scan(node, env, path):
#    return
#bamscan = Scanner(function = bamfile_scan, skeys = ['.bam'])
#env.Append(SCANNERS = bamscan)

############

#ASSEMBLY = "/home/assembly/dev_150/assemblies/allpaths_lg_sample_heinz_raw/sl/data/run/ASSEMBLIES/test/final.assembly.fasta"
#PREFIX   = "allpaths_lg"

#ASSEMBLY = "/home/assembly/dev_150/assemblies/clc-default/clc_contigs.fa"
#PREFIX   = "clc"

#ASSEMBLY = "/home/assembly/dev_150/assemblies/allpaths_lg_sample_heinz_raw_with454/sl/data/run/ASSEMBLIES/test/final.assembly.fasta"
#PREFIX   = "allpaths_lg_454"

ASSEMBLY = "/home/assembly/dev_150/assemblies/S_lycopersicum_scaffolds.2.40.fa"
PREFIX   = "heinz"

#ASSEMBLY = "/home/assembly/progs/fermi/heinz/fmdef.p4.fa"
#PREFIX   = "fermi"

indexFile             = "SConstruct.cfg"
MIN_PE_INS            =  320
MAX_PE_INS            =  430
MIN_MP_INS            = 2435
MAX_MP_INS            = 3555
ESTIMATED_GENOME_SIZE = 760000000
OUTPUT_HEADER         = PREFIX + '.FRC.'


def addCommand(cmd, data):
    src    = data['SOURCE']
    tgt    = data['TARGET']

    allOk = True
    for key in data:
        val = data[key]
        if isinstance(val, list) or str(type(val)) == "<class 'SCons.Node.NodeList'>":
            valStr    = " ".join([str(x) for x in val])
            data[key] = valStr
            #print "  CONVERTING %s FROM %s TO %s" % (key, val, valStr)

            if key == "TARGET":
                for fileName in val:
                    if not os.path.exists(str(val)):
                        print "      TARGET %s DOES NOT EXISTS" % str(fileName)
                        allOk = False

        else:
            #print "  NOT LIST %s VAL %s ('%s')" % (key, val, type(val))
            if key == "TARGET" and not os.path.exists(str(val)):
                print "      TARGET %s DOES NOT EXISTS" % str(val)
                allOk = False


    #if allOk and ONLY_PROCESS_IF_DONT_EXISTS:
    #    if isinstance(tgt, list) or str(type(tgt)) == "<class 'SCons.Node.NodeList'>":
    #        return tgt
    #    else:
    #        return [ tgt ]

    cmdStr = cmd % data
    print "      CMD STRING", cmdStr
    print "       ", "\n        ".join(["%s: %s"%(x, data[x]) for x in data]), "\n\n"
    res = env.Command(tgt, src, cmdStr)
    Decider('timestamp-match')
    return res

#DATABASE
bwtFile = "%s.bwt" % PREFIX
bwtRes  = addCommand("bwa index -a is -p %(BWT_PREFIX)s %(SOURCE)s", {"BWT_PREFIX": PREFIX, "TARGET": bwtFile, "SOURCE": ASSEMBLY})


#SETUP
data     = {}
pairData = {}
with open(indexFile, 'r') as i:
    for line in i:
        if     line[0]       == "#": continue
        if len(line.strip()) == 0  : continue
        fileType, pairName, fileName = line.strip().split("\t")

        if fileType not in data:
            data[fileType] = {}

        data[fileType][fileName] = [[],[]]

        if fileType not in pairData:
            pairData[fileType] = {}

        if pairName not in pairData[fileType]:
            pairData[fileType][pairName] = []

        pairData[fileType][pairName].append(fileName)

#print "DATA", data
#print "PAIR", pairData

oks      = []
resMerge = {}
for fileType in data:
    print "FILE TYPE", fileType
    for fileName in data[fileType]:
        print "  FILE NAME", fileName
        bn      = os.path.basename(fileName).replace('.fastq', '')
        mapName = "%s.%s.sai" % ( PREFIX, bn )
        res     = [ fileName ]

        if fileType == "MP":
            outName = "rc_%s" % bn

            #Convert the mate pair data into paired-end format.
            res     = addCommand("revseq -sequence %(SOURCE)s -outseq %(TARGET)s -notag", {"TARGET": outName+'.fastq', "SOURCE": fileName, 'OUTFILE': outName})
            mapName = "%s.rc_%s.sai" % ( PREFIX, bn )
            print "   ", fileType , "REVSEQ", fileName, ">", [str(x) for x in res]

        print "   ", fileType , "BWA IN ",[str(x) for x in res    ]
        mpMap   = addCommand("bwa aln -t %(NUM_THREADS)d %(PREFIX)s %(SOURCE)s > %(TARGET)s", {"NUM_THREADS":num_threads, "TARGET": mapName, "SOURCE": res, "PREFIX": PREFIX})
        print "   ", fileType , "BWA OUT",[str(x) for x in mpMap]
	Depends( mpMap, bwtRes )        
	data[fileType][fileName][0] = res
        data[fileType][fileName][1] = mpMap



    pairBams = []
    for pairName in pairData[fileType]:
        fqs  = []
        maps = []

        for fileName in pairData[fileType][pairName]:
            fqs.extend( data[fileType][fileName][0])
            maps.extend(data[fileType][fileName][1])

        if pairName != "":
            pairName = pairName + '.'

        if fileType == "MP":
            pairName = 'rc_' + pairName
        if fileType == "PE":
            pairName = 'PE.' + pairName

        print "   ", fileType , "FQS ", [str(x) for x in fqs ]
        print "   ", fileType , "MAPS", [str(x) for x in maps]

        # Create SAM files of the paired mappings
        outSam = "%s.%ssam" % ( PREFIX, pairName )
        inSam  = maps+fqs
        print "   ", fileType , "SAMPE IN ",[str(x) for x in inSam]

        resSam = addCommand("bwa sampe -P %(PREFIX)s %(SOURCE)s > %(TARGET)s", {'PREFIX': PREFIX, 'SOURCE': inSam, 'TARGET': outSam})
        print "   ", fileType , "SAMPE OUT", [str(x) for x in resSam]
        Depends( resSam, bwtRes )


        # Convert result to BAM
        outBam = "%s.%sbam" % ( PREFIX, pairName )
        resBam = addCommand("samtools view -S %(SOURCE)s -b > %(TARGET)s", {'TARGET': outBam, 'SOURCE': resSam})
        print "   ", fileType , "BAM OUT", [str(x) for x in resBam]

        # Sort the BAM
        outBamSort      = outBam.replace('.bam', '') + '.sorted.bam'
        outBamSortShort = outBam.replace('.bam', '') + '.sorted'
        resBamSort      = addCommand("samtools sort %(SOURCE)s %(TARGETSHORT)s", { 'TARGET': outBamSort, 'SOURCE': resBam, 'TARGETSHORT': outBamSortShort })
        print "   ", fileType , "BAM SORT OUT", [str(x) for x in resBamSort]

        # Index the BAM
        outBamIndex      = outBamSort + '.bai'
        resBamSortIndex  = addCommand("samtools index %(SOURCE)s", {'TARGET': outBamIndex, 'SOURCE': resBamSort})
        print "   ", fileType , "BAM SORT INDEX OUT", [str(x) for x in resBamSortIndex]

        # create ok file to create dependency
        #resAll = resSam+resBam+resBamSort+resBamSortIndex
        #print "   ", fileType , "RES ALL IN",[str(x) for x in resAll]
        #outOk = "%s.%sok" % ( PREFIX, pairName )
        #resOk = addCommand("echo %(SOURCE)s > %(TARGET)s", {'TARGET': outOk, 'SOURCE': resAll})
        #oks.extend(resOk)

        pairBams.extend(resBamSort)
        #print "   ", fileType , "PAIR BAMS",[str(x) for x in pairBams]

    # Create SAM files of the paired mappings
    if len(pairBams) > 1:
        outMergePair       = "%s.%s.bam" % ( PREFIX, fileType )
        print "   ", fileType , "MERGE PAIR BAMS IN ",[str(x) for x in pairBams       ]
        outMergePairRes    = addCommand("samtools merge -r -1 %(TARGET)s %(SOURCE)s", {'TARGET': outMergePair, 'SOURCE': pairBams})
        print "   ", fileType , "MERGE PAIR BAMS OUT",[str(x) for x in outMergePairRes]
        resMerge[fileType] = outMergePairRes
    else:
        #print "   ", fileType , "MERGE PAIR BAMS OUT",[str(x) for x in outMergePair]
        resMerge[fileType] = pairBams



# create ok file to create dependency
#print " RES OKS",[str(x) for x in oks]
#outOkAll = "%s.%s.ok" % ( PREFIX, 'all' )
#resOkAll = addCommand("echo %(SOURCE)s > %(TARGET)s", {'TARGET': outOkAll, 'SOURCE': oks})


# run FRC as per README
#FRC --pe-sam ${PREFIX}.PE.sorted.bam \
#	--pe-min-insert $MIN_PE_INS \
#	--pe-max-insert $MAX_PE_INS \
#	--mp-sam ${PREFIX}.MP.bam \
#	--mp-min-insert $MIN_MP_INS \
#	--mp-max-insert $MAX_MP_INS \
#	--genome-size $ESTIMATED_GENOME_SIZE \
#	--output $OUTPUT_HEADER
outFRC = OUTPUT_HEADER + '_FRC.txt'
FCRcmd = "FRC \
         --pe-sam %(pesam)s \
         --pe-min-insert %(peminins)d \
         --pe-max-insert %(pemaxins)d \
         --mp-sam %(mpsam)s \
         --mp-min-insert %(mpminins)d \
         --mp-max-insert %(mpmaxins)d \
         --genome-size %(gensize)d \
         --output %(output)s" %    {
                                        'pesam'   : resMerge['PE'][0],
                                        'peminins': MIN_PE_INS,
                                        'pemaxins': MAX_PE_INS,

                                        'mpsam'   : resMerge['MP'][0],
                                        'mpminins': MIN_MP_INS,
                                        'mpmaxins': MAX_MP_INS,

                                        'gensize' : ESTIMATED_GENOME_SIZE,
                                        'output'  : OUTPUT_HEADER
                                    }
print FCRcmd
allMerged = []
for x in resMerge:
    allMerged.extend(resMerge[x])

outMerged = addCommand(FCRcmd, {'TARGET': outFRC, 'SOURCE': allMerged })


# create ok file to create dependency
#outOkEnd = "%s.%s.ok" % ( PREFIX, 'end' )
#resOkEnd = addCommand("echo %(SOURCE)s > %(TARGET)s", {'TARGET': outOkEnd, 'SOURCE': resOkAll + outMerged})


Default(env.Alias('all'))
#env.Alias( 'all', resOkEnd )
env.Alias( 'all', outMerged )



def bf_to_str(bf):
    """Convert an element of GetBuildFailures() to a string
    in a useful way."""
    import SCons.Errors
    if bf is None: # unknown targets product None in list
        return '(unknown tgt)'
    elif isinstance(bf, SCons.Errors.StopError):
        return str(bf)
    elif bf.node:
        return str(bf.node) + ': ' + bf.errstr                              + "\nEXECUTOR: " + str(bf.executor) + "\nACTION: " + str(bf.action)
    elif bf.filename:
        return bf.filename  + ': ' + bf.errstr + "\nCOMMAND: " + bf.command + "\nEXECUTOR: " + str(bf.executor) + "\nACTION: " + str(bf.action)

    return 'unknown failure: ' + bf.errstr


def build_status():
    """Convert the build status to a 2-tuple, (status, msg)."""
    from SCons.Script import GetBuildFailures
    bf = GetBuildFailures()
    if bf:
        # bf is normally a list of build failures; if an element is None,
        # it's because of a target that scons doesn't know anything about.
        status = 'failed'
        failures_message = "\n".join(["Failed building %s" % bf_to_str(x)
                           for x in bf if x is not None])
    else:
        # if bf is None, the build completed successfully.
        status = 'ok'
        failures_message = ''
    return (status, failures_message)


def display_build_status():
    """Display the build status.  Called by atexit.
    Here you could do all kinds of complicated things."""
    status, failures_message = build_status()
    if status == 'failed':
       print "FAILED!!!!"  # could display alert, ring bell, etc.
    elif status == 'ok':
       print "Build succeeded."
    print failures_message

atexit.register(display_build_status)
#
#sys.exit(0)
